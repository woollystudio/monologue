# Monologue

Simple stereo wav trigger player.

![](monologue-interface.png)

# Use

You must download [Cycling'Max](https://cycling74.com/downloads) first to use this software.

Put your media files (.wav) in the media/ directory. Cues's order follows the alphanumeric classification.

Cheat sheet, look at the screenshot above.

Bests.