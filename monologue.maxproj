{
	"name" : "monologue",
	"version" : 1,
	"creationdate" : 3762864314,
	"modificationdate" : 3762876919,
	"viewrect" : [ 25.0, 106.0, 300.0, 500.0 ],
	"autoorganize" : 0,
	"hideprojectwindow" : 1,
	"showdependencies" : 1,
	"autolocalize" : 1,
	"contents" : 	{
		"patchers" : 		{
			"main.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}

		}
,
		"media" : 		{
			"Anaïs-#0.0.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"Fin-#0.0.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"Intro-#0.0.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"La-légende-#0.0.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"Recette-sauce-#0.0.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"Recette-tortillas-#0.0.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0,
	"includepackages" : 0
}
